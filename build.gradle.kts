import ca.stellardrift.build.configurate.ConfigFormats
import ca.stellardrift.build.configurate.transformations.convertFormat

plugins {
    val opinionatedVersion = "5.0.0"
    val indraVersion = "2.0.6"
    id("ca.stellardrift.opinionated.fabric") version opinionatedVersion
    id("ca.stellardrift.configurate-transformations") version opinionatedVersion
    id("net.kyori.indra.publishing.sonatype") version indraVersion
}

group = "ca.stellardrift"
version = "0.2.2"
description = "An extension to Minecraft's command system to allow client-optional custom argument types"

repositories {
    maven("https://repo.stellardrift.ca/repository/stable/") {
        name = "stellardrift"
        mavenContent { releasesOnly() }
    }

    maven("https://repo.stellardrift.ca/repository/snapshots/") {
        name = "stellardrift"
        mavenContent { snapshotsOnly() }
    }
}

indra {
    gitlab("stellardrift", "colonel") {
        ci(true)
    }
    apache2License()

    configurePublications {
        pom {
            developers {
                developer {
                    id.set("zml")
                    name.set("zml")
                    timezone.set("America/Vancouver")
                    email.set("zml [at] stellardrift . ca")
                }
            }
        }
    }

    publishReleasesTo("stellardrift", "https://repo.stellardrift.ca/repository/releases/")
    publishSnapshotsTo("stellardrift", "https://repo.stellardrift.ca/repository/snapshots/")
}

val versionMinecraft: String by project
val versionYarn: String by project
val versionLoader: String by project
val versionFabricApi: String by project

dependencies {
    minecraft("com.mojang:minecraft:$versionMinecraft")
    mappings("net.fabricmc:yarn:$versionMinecraft+build.$versionYarn:v2")
    modImplementation("net.fabricmc:fabric-loader:$versionLoader")

    annotationProcessor("com.google.auto.value:auto-value:1.8.2")
    compileOnly("com.google.auto.value:auto-value-annotations:1.8.2")
    modApi("org.checkerframework:checker-qual:3.17.0")

    // fapi -- optional
    modImplementation("net.fabricmc.fabric-api:fabric-api:$versionFabricApi")
}

tasks.withType(ProcessResources::class).configureEach {
    filesMatching("fabric.mod.yml") {
        expand("project" to project)
        convertFormat(ConfigFormats.YAML, ConfigFormats.JSON)
        name = "fabric.mod.json"
    }

    filesMatching("*.mixins.json") {
        expand("project" to project)
    }
}
